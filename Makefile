install-venv: .venv/bin/activate

.venv/bin/activate: requirements.txt
	test -d .venv || virtualenv -p python3.6 --prompt="(synker) " .venv
	. .venv/bin/activate; pip install -r requirements.txt
	touch .venv/bin/activate

clean: clean-build
	rm -rf .venv

clean-build:
	rm -rf __pycache__
	rm -rf build
	rm -rf dist
	rm -rf *.egg-info

test: .venv/bin/activate
	. .venv/bin/activate; python -m unittest discover

test-build: .venv/bin/activate
	. .venv/bin/activate; python setup.py build

import unittest
import os
import logging
from unittest.mock import patch, Mock
from tempfile import TemporaryDirectory
from time import strptime
from calendar import timegm
from os.path import dirname, abspath, join, exists, expanduser, isfile, isdir
from synker import (
    load_config_file,
    clean_path,
    remove_path,
    execute,
    rsync,
    rsync_ssh,
    mysql_dump,
    build_tarball,
    build_archive,
    parse_tar_file_name,
    list_history_dir,
    normalize_schedule,
    exec_options,
    get_home_dir,
    )


ROOT_DIR = dirname(abspath(__file__))  # root is where this file is located

TEST_DATA_DIR = abspath(join(ROOT_DIR, 'data'))


class TestSynker(unittest.TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)
        self._temporary_dir = TemporaryDirectory()
        self.tmp_dir = self._temporary_dir.__enter__()
        assert exists(self.tmp_dir)

    def tearDown(self):
        logging.disable(logging.NOTSET)
        self._temporary_dir.__exit__(None, None, None)
        assert not exists(self.tmp_dir)

    def test_01_load_config_file(self):

        path = join(TEST_DATA_DIR, 'test_conf.py')
        d = load_config_file(path)
        self.assertEqual(d, {
            'test_smart_schedule': {
                'archives': ['out/'],
                'schedule': 'smart'}})

        with self.assertRaises(OSError):
            load_config_file('nonsense/path')

    def test_02_clean_path(self):

        cwd = os.getcwd()
        home = expanduser('~')

        path = clean_path('/this/path')
        self.assertEqual(path, '/this/path')

        path = clean_path('./this/path')
        self.assertEqual(path, join(cwd, 'this', 'path'))

        path = clean_path('this/path')
        self.assertEqual(path, join(cwd, 'this', 'path'))

        path = clean_path('~/this/path')
        self.assertEqual(path, join(home, 'this', 'path'))

    def test_03_remove_path(self):

        path1 = join(self.tmp_dir, 'foo')
        path2 = join(path1, 'bar')
        path3 = join(path1, 'file.txt')
        os.makedirs(path2)
        with open(path3, 'w') as f:
            pass
        self.assertTrue(exists(path1))
        self.assertTrue(exists(path2))
        self.assertTrue(exists(path3))

        remove_path(path3)
        self.assertTrue(exists(path1))
        self.assertTrue(exists(path2))
        self.assertFalse(exists(path3))

        remove_path(path1)
        self.assertFalse(exists(path1))
        self.assertFalse(exists(path2))
        self.assertFalse(exists(path3))

    def test_04_execute(self):

        stdout, stderr, exitcode = execute('echo "Hello World"')
        self.assertEqual(stdout, 'Hello World\n')
        self.assertEqual(stderr, '')
        self.assertEqual(exitcode, 0)

        stdout, stderr, exitcode = execute('test -d ' + TEST_DATA_DIR)
        self.assertEqual(stdout, '')
        self.assertEqual(stderr, '')
        self.assertEqual(exitcode, 0)

        with patch('synker.warning') as mock_warning:
            stdout, stderr, exitcode = execute('test -d nonsense/path')
            self.assertEqual(mock_warning.call_count, 1)
        self.assertEqual(stdout, '')
        self.assertEqual(stderr, '')
        self.assertEqual(exitcode, 1)

    def test_05_rsync(self):

        dst_path = join(self.tmp_dir, 'out')
        src_path = join(TEST_DATA_DIR, 'my_files')

        stdout, stderr, exitcode = rsync(dst_path, src_path)
        self.assertEqual(exitcode, 0)
        self.assertTrue(exists(dst_path))
        self.assertTrue(exists(join(dst_path, 'my_files')))
        self.assertTrue(exists(join(dst_path, 'my_files', 'file01.txt')))
        self.assertTrue(exists(join(dst_path, 'my_files', 'file02.txt')))
        self.assertTrue(exists(join(dst_path, 'my_files', 'file03.txt')))

    def test_06_rsync_ssh(self):

        obj = Mock()
        obj.communicate = Mock(return_value=('', ''))
        obj.returncode = 0
        with patch('synker.subprocess.Popen', return_value=obj) as mock_cls:
            rsync_ssh('/dst/path', 'myuser', 'myserver', '/src/path', '22')
            command_string = mock_cls.call_args[0][0]
            self.assertEqual(
                command_string,
                'rsync -avz --delete -e "ssh -p 22" '
                'myuser@myserver:/src/path /dst/path')

    def test_07_mysql_dump(self):

        obj = Mock()
        obj.communicate = Mock(return_value=('', ''))
        obj.returncode = 0
        with patch('synker.subprocess.Popen', return_value=obj) as mock_cls:
            mysql_dump('/dst/path', 'mydb', 'myuser', 'mypass', 'localhost')
            command_string = mock_cls.call_args[0][0]
            self.assertEqual(
                command_string,
                'mysqldump mydb -u myuser -pmypass -h localhost > /dst/path')

    def test_08_build_tarball(self):

        dst_path = join(self.tmp_dir, 'out.tar.gz')
        src_path = join(TEST_DATA_DIR, 'my_files')
        stdout, stderr, exitcode = build_tarball(dst_path, src_path)
        self.assertEqual(exitcode, 0)
        self.assertTrue(exists(dst_path))
        self.assertTrue(isfile(dst_path))

    def test_09_build_archive(self):

        sync_dir = self.tmp_dir

        # a local folder with folders and files
        local = {
            'A': join(TEST_DATA_DIR, 'my_files'),
            'B': join(TEST_DATA_DIR, 'other_files'),
            'src01.txt': join(TEST_DATA_DIR, 'src01.txt'),
            'src02.txt': join(TEST_DATA_DIR, 'src02.txt'),
            }
        build_archive(sync_dir, local, {}, {})
        local_dir = join(sync_dir, 'local')
        self.assertTrue(isdir(join(local_dir, 'A', 'my_files')))
        self.assertTrue(isfile(join(local_dir, 'A', 'my_files', 'file01.txt')))
        self.assertTrue(isfile(join(local_dir, 'A', 'my_files', 'file02.txt')))
        self.assertTrue(isfile(join(local_dir, 'A', 'my_files', 'file03.txt')))
        self.assertTrue(isdir(join(local_dir, 'B', 'other_files')))
        self.assertTrue(
            isfile(join(local_dir, 'B', 'other_files', 'otherfile01.txt')))
        self.assertTrue(
            isfile(join(local_dir, 'B', 'other_files', 'otherfile02.txt')))
        self.assertTrue(
            isfile(join(local_dir, 'B', 'other_files', 'otherfile03.txt')))
        self.assertTrue(isfile(join(local_dir, 'src01.txt')))
        self.assertTrue(isfile(join(local_dir, 'src02.txt')))

        # remove some things
        del local['B']
        del local['src02.txt']
        with patch('synker.warning') as mock_warning:
            build_archive(sync_dir, local, {}, {})
            self.assertEqual(mock_warning.call_count, 2)
        local_dir = join(sync_dir, 'local')
        self.assertTrue(isdir(join(local_dir, 'A', 'my_files')))
        self.assertTrue(isfile(join(local_dir, 'A', 'my_files', 'file01.txt')))
        self.assertTrue(isfile(join(local_dir, 'A', 'my_files', 'file02.txt')))
        self.assertTrue(isfile(join(local_dir, 'A', 'my_files', 'file03.txt')))
        self.assertFalse(exists(join(local_dir, 'B', 'other_files')))
        self.assertTrue(isfile(join(local_dir, 'src01.txt')))
        self.assertFalse(exists(join(local_dir, 'src02.txt')))

    def test_10_parse_tar_file_name(self):

        name, minutes = parse_tar_file_name('test_2000-01-01_00-00-00.tar.gz')
        self.assertEqual(name, 'test')
        self.assertEqual(minutes, 15778080)

        name, minutes = parse_tar_file_name(
            'test_2_2002-06-13_23-11-34.tar.gz')
        self.assertEqual(name, 'test_2')
        self.assertEqual(minutes, 17066831)

        name, minutes = parse_tar_file_name(
            'test_abc_1234_2011-10-23_21-59-01.tar.gz')
        self.assertEqual(name, 'test_abc_1234')
        self.assertEqual(minutes, 21990119)

        out = parse_tar_file_name('nonsense-13_23-11-34.tar.gz')
        self.assertEqual(out, None)

    def test_11_list_history_dir(self):

        history_dir = join(TEST_DATA_DIR, 'history')
        out = list_history_dir(history_dir)
        self.assertEqual(out, [
            (join(history_dir, 'empty_2000-07-20_12-15-33.tar.gz'),
                'empty', 16068255),
            (join(history_dir, 'empty_2000-07-20_06-00-00.tar.gz'),
                'empty', 16067880),
            (join(history_dir, 'empty_2000-07-20_00-00-00.tar.gz'),
                'empty', 16067520),
            (join(history_dir, 'empty_2000-06-23_23-00-00.tar.gz'),
                'empty', 16030020),
            ])

    def test_12_normalize_schedule(self):

        schedule = [
            (0, 100),
            (5, 100),
            (3, 50),
            (-1, 60),
            (0, 80),
            (10, 0),
            (5, 90),
            (10, 40),
            (10, 70),
            (12, 50),
            (3, 100),
            (3, 200),
            (3, 400),
            ]
        out = normalize_schedule(schedule)
        self.assertEqual(out, [
            (10, 40),
            (12, 50),
            (10, 70),
            (5, 90),
            (5, 100),
            (3, 200),
            (3, 400),
            ])

    def test_13_exec_options(self):

        # create a test config file
        config_path = join(self.tmp_dir, 'test_conf.py')
        archive_path = join(self.tmp_dir, 'archive')
        os.mkdir(archive_path)
        with open(config_path, 'w') as f:
            f.write(str({
                'myproject': {
                    'schedule': [
                        (3, 1440),  # keep at most 3 daily backups
                        (4, 10080),  # followed by at most 4 weekly backups
                        (5, 43200),  # followed by at most 5 30-day backups
                        ],
                    'archives': [archive_path],
                    'local': {'my_files': join(TEST_DATA_DIR, 'my_files')},
                }}))

        # create the user arguments
        options = {
            '--verbose': False,
            '--config': config_path,
            '--time': int(timegm(strptime('2000-01-01', '%Y-%m-%d')) / 60),
            '--force': False,
            }

        # run routine once a day for 180 days
        self.assertFalse(exists(join(archive_path, 'myproject')))
        for i in range(180):
            exec_options(options)
            options['--time'] += 1440
        self.assertTrue(exists(join(archive_path, 'myproject')))

        # compare directory structures
        expected = [
            'myproject_2000-01-01_00-00-00.tar.gz',
            'myproject_2000-02-05_00-00-00.tar.gz',
            'myproject_2000-03-11_00-00-00.tar.gz',
            'myproject_2000-04-15_00-00-00.tar.gz',
            'myproject_2000-05-20_00-00-00.tar.gz',
            'myproject_2000-06-03_00-00-00.tar.gz',
            'myproject_2000-06-10_00-00-00.tar.gz',
            'myproject_2000-06-17_00-00-00.tar.gz',
            'myproject_2000-06-24_00-00-00.tar.gz',
            'myproject_2000-06-26_00-00-00.tar.gz',
            'myproject_2000-06-27_00-00-00.tar.gz',
            'myproject_2000-06-28_00-00-00.tar.gz',
            ]
        actual = sorted(os.listdir(join(archive_path, 'myproject')))
        self.assertEqual(expected, actual)

    def test_14_exec_options_smart(self):

        # create a test config file
        config_path = join(self.tmp_dir, 'test_conf.py')
        archive_path = join(self.tmp_dir, 'archive')
        os.mkdir(archive_path)
        with open(config_path, 'w') as f:
            f.write(str({
                'myproject': {
                    'archives': [archive_path],
                    'schedule': 'smart',
                    'local': {'my_files': join(TEST_DATA_DIR, 'my_files')},
                }}))

        # create the user arguments
        options = {
            '--verbose': False,
            '--config': config_path,
            '--time': int(timegm(strptime('2000-01-01', '%Y-%m-%d')) / 60),
            '--force': False,
            }

        self.assertFalse(exists(join(archive_path, 'myproject')))
        exec_options(options)
        self.assertTrue(exists(join(archive_path, 'myproject')))
        expected = ['myproject_2000-01-01_00-00-00.tar.gz']
        actual = os.listdir(join(archive_path, 'myproject'))
        self.assertEqual(expected, actual)

    def test_15_get_home_dir(self):

        path = get_home_dir()
        self.assertEqual(path, join(expanduser('~'), '.synker'))

    def test_16_force_backup(self):

        # create a test config file
        config_path = join(self.tmp_dir, 'test_conf.py')
        archive_path = join(self.tmp_dir, 'archive')
        os.mkdir(archive_path)
        with open(config_path, 'w') as f:
            f.write(str({
                'myproject': {
                    'archives': [archive_path],
                    'schedule': [(10, 43200)],  # 30-day backups
                    'local': {'my_files': join(TEST_DATA_DIR, 'my_files')},
                }}))

        # create the user arguments
        options = {
            '--verbose': False,
            '--config': config_path,
            '--time': int(timegm(strptime('2000-01-01', '%Y-%m-%d')) / 60),
            '--force': False,
            }

        self.assertFalse(exists(join(archive_path, 'myproject')))

        exec_options(options)
        self.assertTrue(exists(join(archive_path, 'myproject')))
        expected = ['myproject_2000-01-01_00-00-00.tar.gz']
        actual = os.listdir(join(archive_path, 'myproject'))
        self.assertEqual(expected, actual)

        # running backup one day later shouldn't do anything
        options['--time'] = int(
            timegm(strptime('2000-01-02', '%Y-%m-%d')) / 60)
        exec_options(options)
        expected = ['myproject_2000-01-01_00-00-00.tar.gz']
        actual = os.listdir(join(archive_path, 'myproject'))
        self.assertEqual(expected, actual)

        # force the backup
        options['--force'] = True
        exec_options(options)
        expected = [
            'myproject_2000-01-01_00-00-00.tar.gz',
            'myproject_2000-01-02_00-00-00.tar.gz',
            ]
        actual = sorted(os.listdir(join(archive_path, 'myproject')))
        self.assertEqual(expected, actual)

    def test_17_independent_backups(self):

        # create a test config file
        config_path = join(self.tmp_dir, 'test_conf.py')
        archive_path = join(self.tmp_dir, 'archive')
        os.mkdir(archive_path)
        with open(config_path, 'w') as f:
            f.write(str({
                'myproject': {
                    'archives': [],
                    'schedule': 'smart',
                    },
                'otherproj': {
                    'archives': [],
                    'schedule': 'smart',
                    },
                }))

        # create the user arguments
        options = {
            '--verbose': False,
            '--config': config_path,
            '--time': None,
            '--force': False,
            }

        # mock method to fail on first project and succeed on second
        effect = (Exception('error'), None)
        with patch('synker.backup_project', side_effect=effect) as mock_backup:
            with patch('synker.log_exception') as mock_log_exc:
                exec_options(options)
        self.assertEqual(mock_log_exc.call_count, 1)
        self.assertEqual(mock_backup.call_count, 2)

#!/usr/bin/env python

"""Usage: synker [options]

A lightweight automated backup utility.

Options:
  -h --help                 display help
  -c --config PATH          path to configuration file
  -f --force                force backup regardless of configured schedule
  -t --time TIME            manually provide the current time in minutes
                            since epoch (useful for testing)
  -v --verbose              print all debug information and tracebacks
  --version                 show version

"""

import os
import sys
import subprocess
import shutil
import re
import logging
import logging.handlers
from docopt import docopt
from textwrap import dedent
from os.path import (
    join, dirname, realpath, expanduser, isfile,
    isdir, split, normpath, exists, basename, isabs
    )
from ast import literal_eval
from time import strftime, strptime, gmtime
from calendar import timegm
from operator import itemgetter
from tempfile import TemporaryDirectory


VERSION = '0.2'

ROOT_DIR = dirname(realpath(__file__))  # root is where this file is located

FMT_DATETIME = '%Y-%m-%d_%H-%M-%S'

RE_DATETIME = re.compile(
    '^(.*)_([0-9]{4}-[0-9]{2}-[0-9]{2}_[0-9]{2}-[0-9]{2}-[0-9]{2}).*')

SCHEDULE_PRESET_SMART = [
    (5, 1440),  # keep at most 5 daily backups
    (5, 10080),  # followed by at most 5 weekly backups
    (10, 43200),  # followed by at most 10 30-day backups
    (50, 144000),  # followed by at most 50 100-day backups
    ]

SCHEDULE_PRESET_DAILY = [
    (9999, 1440),  # keep a LOT of daily backups
    ]

SCHEDULE_PRESET_WEEKLY = [
    (9999, 10080),  # keep a LOT weekly backups
    ]


# logging methods
synker_logger = logging.getLogger(__name__)
debug = synker_logger.debug
warning = synker_logger.warning
error = synker_logger.error


def get_version():
    """Return the project's version string.
    """
    return VERSION


def clean_path(path):
    """Return the path in a normalized form.
    """
    return realpath(expanduser(path))


def get_home_dir():
    """Return the path to the user's home directory.
    """
    return clean_path(join('~', '.synker'))


def get_default_config_path():
    """Return the path to the user's default configuration file.
    """
    return join(get_home_dir(), 'conf.py')


def make_default_config_file(path):
    """Save a default configuration file at the given path.
    """
    assert isabs(path)
    assert not exists(path)
    contents = """
        {
            "project": {

                ## List of target directories to save the new backup files.
                "archives": [
                    ## "/path/to/archive/dir/",
                ],

                ## Configure the backup schedule or use a preset:
                ## "smart", "daily", "weekly".
                "schedule": "smart",

                ## Dict of local system paths to include in the backup.
                "local": {
                    ## "name": "/path/to/source",
                },

                ## Dict of MySQL dumps to include in the backup.
                "mysql": {
                    ## "name": ("database", "username", "password", "host"),
                },

                ## Dict of remote paths (via SSH) to include in the backup.
                "ssh": {
                    ## "name": ("user", "host", "/path/to/source", "port"),
                },
            },
        }
        """
    with open(path, 'w') as f:
        f.write(dedent(contents).lstrip())


def load_config_file(path):
    """Evaluate a configuration file and return it as a python dict.
    """
    with open(path, 'r') as f:
        return literal_eval(f.read())


def remove_path(path):
    """Remove file or directory from system.
    """
    if isfile(path):
        os.remove(path)
    elif isdir(path):
        shutil.rmtree(path)


def execute(command_string):
    """Execute the given command string as a subprocess.
    """
    proc = subprocess.Popen(
        command_string,
        shell=True,
        stdout=subprocess.PIPE,
        stdin=subprocess.PIPE,
        stderr=subprocess.PIPE,
        universal_newlines=True,
        )
    stdout, stderr = proc.communicate()
    # TODO: log stdout, stderr
    exitcode = proc.returncode
    if exitcode != 0:
        warning('The following command returned {} upon exit: {}'.format(
            exitcode, command_string))
    return stdout, stderr, exitcode


def rsync(dst_path, src_path):
    """Use the 'rsync' utility to copy a file or directory.
    """
    return execute('rsync -av --delete {} {}'.format(src_path, dst_path))


def rsync_ssh(dst_path, username, hostname, src_path, port):
    """Use the 'rsync' utility to copy a file or directory from a remote host.
    """
    return execute('rsync -avz --delete -e "ssh -p {}" {}@{}:{} {}'.format(
        port, username, hostname, src_path, dst_path))


def mysql_dump(dst_path, database, username, password, hostname):
    """Use the 'mysqldump' utility to export a MySQL database.
    """
    return execute('mysqldump {} -u {} -p{} -h {} > {}'.format(
        database, username, password, hostname, dst_path))


def build_tarball(dst_path, src_path):
    """Build a tarball from a source directory.
    """
    (head, tail) = split(normpath(src_path))
    return execute('tar -zcvf {} -C {} {}'.format(dst_path, head, tail))


def build_archive(sync_dir, local, mysql, ssh):
    """Build an archive directory according to the supplied settings.
    """

    # ensure target folders exists
    local_dir = join(sync_dir, 'local')
    mysql_dir = join(sync_dir, 'mysql')
    ssh_dir = join(sync_dir, 'ssh')
    for p in [local_dir, mysql_dir, ssh_dir]:
        if not exists(p):
            os.mkdir(p)

    # keep a list of project items
    all_project_items = [local_dir, mysql_dir, ssh_dir]

    # rsync all local files/ folders
    for n, p in local.items():
        dst = join(local_dir, n)
        rsync(dst, p)
        all_project_items.append(dst)

    # do all mysql dumps
    for n, t in mysql.items():
        dst = join(mysql_dir, n)
        mysql_dump(dst, *t)
        all_project_items.append(dst)

    # rsync all remote (ssh) files/ folders
    for n, t in ssh.items():
        dst = join(ssh_dir, n)
        rsync_ssh(dst, *t)
        all_project_items.append(dst)

    # remove any files/ folders that don't belong
    items = [join(sync_dir, f) for f in os.listdir(sync_dir)]
    items.extend([join(local_dir, f) for f in os.listdir(local_dir)])
    items.extend([join(mysql_dir, f) for f in os.listdir(mysql_dir)])
    items.extend([join(ssh_dir, f) for f in os.listdir(ssh_dir)])
    for p in items:
        if p not in all_project_items:
            remove_path(p)
            warning('Removing item from build tree: {}'.format(p))


def parse_tar_file_name(tar_file):
    """Extract name and datetime information from a filename.
    """

    name = basename(tar_file)
    result = RE_DATETIME.match(name)
    if result is None:
        return None

    name = result.group(1)
    datetime = result.group(2)
    minutes = int(timegm(strptime(datetime, FMT_DATETIME)) / 60)
    return (name, minutes)


def list_history_dir(history_dir):
    """Return a list of the contents of a history directory.
    """

    # attempt to parse all files
    history = []
    for p in [join(history_dir, f) for f in os.listdir(history_dir)]:

        if not isfile(p):
            continue
        parts = parse_tar_file_name(p)
        if parts is None:
            continue
        history.append((p, parts[0], parts[1]))

    # sort by minutes (descending)
    history.sort(key=itemgetter(2), reverse=True)
    return history


def normalize_schedule(schedule):
    """Normalize the format of the given schedule by removing meaningless
    values and sorting the remainder.
    """

    # deep copy, removing meaningless values
    schedule = [(x[0], x[1]) for x in schedule if x[0] > 0 and x[1] > 0]
    schedule.sort(key=itemgetter(0), reverse=True)  # sort by units
    schedule.sort(key=itemgetter(1))  # sort by minutes

    # remove duplicate minute values, favoring the one
    # with the higher unit value
    prev = -1
    for s in list(schedule):
        if s[1] == prev:
            schedule.remove(s)
        prev = s[1]

    return schedule


def update_history_dir(history_dir, new_tar_file, schedule, force):
    """Try to add a new tarball file to a history directory according
    to the given schedule.
    """

    # add the new tar file if necessary, return if not necessary
    history = list_history_dir(history_dir)
    new_selector, new_minutes = parse_tar_file_name(new_tar_file)
    overdue = (
        len(history) == 0 or (new_minutes - history[0][2] >= schedule[0][1]))
    if overdue or force:
        debug('Copying new tar file to history directory: {}'.format(
            history_dir))
        shutil.copy(new_tar_file, history_dir)
    else:
        return

    # refresh history listing
    history = list_history_dir(history_dir)

    # create timeframe bins according to schedule
    now = history[0][2] + schedule[0][1]
    bins = []
    for its, delta in schedule:
        bins.extend([now - (i+1)*delta for i in range(its)])
        now = bins[-1]

    # fill bins, removing extras
    bin_contents = [None for b in bins]
    bin_index = 0
    for path, selector, minutes in history:

        # find appropriate bin for current history item
        while minutes < bins[bin_index] and bin_index + 1 < len(bins):
            bin_index += 1

        if minutes < bins[bin_index]:
            debug('Removing file from history: {}'.format(path))
            remove_path(path)
        else:
            # remove previous bin contents (if any)
            old_path = bin_contents[bin_index]
            if old_path is not None:
                debug('Removing file from history: {}'.format(old_path))
                remove_path(old_path)
            bin_contents[bin_index] = path


def backup_project(name, config, working_dir, options):
    """Backup the given project.
    """

    # extract settings
    archives = config['archives']
    schedule = config['schedule']
    local = config['local']
    mysql = config['mysql']
    ssh = config['ssh']
    force = options['--force']

    # schedule must be non-empty
    if len(schedule) == 0 and not force:
        debug('Schedule for "{}" is empty. Skipping.'.format(name))
        return

    # setup file/ folder names
    history_directories = [join(a, name) for a in archives]
    user_time = validate_user_time(options)
    if user_time and user_time > 0:
        dt = strftime(FMT_DATETIME, gmtime(60 * user_time))
    else:
        dt = strftime(FMT_DATETIME, gmtime())
    tar_file = join(working_dir, '{}_{}.tar.gz'.format(name, dt))

    # only build archive if it's needed
    needed = False
    for history_dir in history_directories:

        if not exists(history_dir):
            needed = True
            break
        history = list_history_dir(history_dir)
        new_selector, new_minutes = parse_tar_file_name(tar_file)
        if len(history) == 0 or (
                new_minutes - history[0][2] >= schedule[0][1]):
            needed = True
            break

    if not (needed or force):
        debug('New backup for "{}" not necessary. Skipping.'.format(name))
        return

    # setup working directories
    debug('Starting backup process for "{}".'.format(name))
    sync_dir = join(working_dir, name)
    assert exists(working_dir)
    assert not exists(sync_dir)
    os.mkdir(sync_dir)

    # sync and create tarball
    debug('Building archive here: {}'.format(sync_dir))
    build_archive(sync_dir, local, mysql, ssh)
    debug('Building tarball here: {}'.format(tar_file))
    build_tarball(tar_file, sync_dir)

    # for each destination archive
    for history_dir in history_directories:

        # ensure history directory exists
        if not exists(history_dir):
            os.mkdir(history_dir)

        # try to add new archive
        update_history_dir(history_dir, tar_file, schedule, force)

    # cleanup working dir
    remove_path(tar_file)
    remove_path(sync_dir)


def exec_options(options):
    """Execute the main configuration.
    """

    # load config
    config_path = validate_config_path(options)
    debug('Loading configuration file: ' + config_path)
    configuration = load_config_file(config_path)

    # extract project configurations
    debug('Extracting project configurations.')
    project_configs = dict()
    for k, v in configuration.items():
        # check for required parameters
        if 'archives' in v and 'schedule' in v:
            # copy project config
            project_configs[k] = dict(v)
    unrecognized = sorted(configuration.keys() - project_configs.keys())
    if len(unrecognized) > 0:
        warning('Invalid configuration keys: ' + ', '.join(unrecognized))

    # normalize project configurations
    for k, v in project_configs.items():

        debug('Normalizing project configuration for "{}".'.format(k))

        # archives
        archives = v['archives']
        if type(archives) is str:
            archives = [archives]
        archives = [clean_path(d) for d in archives]
        for d in archives:
            # must be an existing directory
            if not isdir(d):
                raise Exception(
                    'Archive path is not a directory: {}'.format(d))
        v['archives'] = archives

        # schedule
        schedule = v['schedule']
        presets = {
            'smart': SCHEDULE_PRESET_SMART,
            'daily': SCHEDULE_PRESET_DAILY,
            'weekly': SCHEDULE_PRESET_WEEKLY,
        }
        if type(schedule) is str:
            schedule = presets.get(schedule.lower(), None)
        try:
            schedule = [(x[0], x[1]) for x in schedule]  # a list of 2-tuples
            for t in schedule:
                if type(t[0]) is not int or type(t[1]) is not int:
                    raise Exception()
        except:
            raise Exception(
                'Invalid schedule format for "{}". A preset name or a '
                'list of integer pairs expected.'.format(k))
        v['schedule'] = normalize_schedule(schedule)

        # local
        local = v.setdefault('local', dict())
        local = dict([(x, clean_path(y)) for x, y in local.items()])
        for p in local.values():
            if not exists(p):
                raise Exception('Local path does not exist: {}'.format(p))
        v['local'] = local

        # mysql
        mysql = v.setdefault('mysql', dict())
        for t in mysql.values():
            # should contain exactly 4 strings
            if (len(t) != 4 or
                    type(t[0]) is not str or
                    type(t[1]) is not str or
                    type(t[2]) is not str or
                    type(t[3]) is not str):
                raise Exception(
                    'Invalid mysql format for "{}". A 4-tuple of '
                    'strings expected.'.format(k))

        # ssh
        ssh = v.setdefault('ssh', dict())
        for t in ssh.values():
            # should contain exactly 4 strings
            if (len(t) != 4 or
                    type(t[0]) is not str or
                    type(t[1]) is not str or
                    type(t[2]) is not str or
                    type(t[3]) is not str):
                raise Exception(
                    'Invalid ssh format for "{}". A 4-tuple of '
                    'strings expected.'.format(k))

    # backup each project
    for k, v in project_configs.items():
        with TemporaryDirectory() as working_dir:
            try:
                backup_project(k, v, working_dir, options)
            except Exception as e:
                log_exception(e)

    # exit
    debug('All project backups complete.')


def validate_config_path(options):
    """Sanitize and return the user's config file path.
    """
    path = options['--config'] or None
    if path:
        path = clean_path(path)
        if not isfile(path):
            raise Exception('Configuration file does not exist: ' + path)
    return path


def validate_user_time(options):
    """Sanitize and return the optional, user-supplied time.
    """
    t = options['--time']
    if not t:
        return None
    try:
        return int(t) or None
    except ValueError:
        raise Exception('Invalid user-given time integer.')


def log_exception(exc):
    """Log the given exception as an error and include
    the traceback as a debug message.
    """
    debug('An exception occured.', exc_info=True)
    error(exc)


def main():
    """Main entry point for program.
    """

    # parse user arguments
    options = docopt(__doc__, version=get_version())

    # logger captures all levels
    synker_logger.setLevel(logging.DEBUG)

    # log handler that prints to screen
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG if options['--verbose'] else logging.WARNING)
    ch.setFormatter(logging.Formatter('%(levelname)s: %(message)s'))
    synker_logger.addHandler(ch)

    # make home directory if necessary
    home_path = get_home_dir()
    try:
        os.mkdir(home_path)
        warning('Made home directory: ' + home_path)
    except FileExistsError:
        pass

    # log handler that prints to file
    fh = logging.handlers.RotatingFileHandler(
        join(home_path, 'synker.log'), maxBytes=200000, backupCount=5)
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(
        logging.Formatter('%(asctime)s: %(levelname)s: %(message)s'))
    synker_logger.addHandler(fh)

    # make default config file if necessary
    if not options['--config']:
        assert exists(home_path)
        config_path = get_default_config_path()
        if not exists(config_path):
            make_default_config_file(config_path)
            warning('Made default configuration file: ' + config_path)
        options['--config'] = config_path

    # run script
    try:
        exec_options(options)
        return 0

    except Exception as e:
        log_exception(e)
        return 1


if __name__ == '__main__':
    sys.exit(main())

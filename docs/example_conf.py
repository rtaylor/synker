{
    "project": {

        ## List of target directories to save the new backup files.
        "archives": [
            ## "/path/to/archive/dir/",
            "/path/to/archive/dir/",
        ],

        ## Configure the backup schedule or use a preset:
        ## "smart", "daily", "weekly".
        "schedule": "smart",

        ## Dict of local system paths to include in the backup.
        "local": {
            ## "name": "/path/to/local/source",
            "myfolder": "/home/myuser/folder",
            "myfile.txt": "/home/myuser/file.txt",
        },

        ## Dict of MySQL dumps to include in the backup.
        "mysql": {
            ## "name": ("database", "username", "password", "host"),
            "mydatabase.sql": ("mydatabase", "myuser", "mypass", "localhost"),
        },

        ## Dict of remote paths (via SSH) to include in the backup.
        "ssh": {
            ## "name": ("user", "host", "/path/to/source", "port"),
            "file.txt": ("myuser", "myserver", "/home/myuser/file.txt", "22"),
        },
    },
}

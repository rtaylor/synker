from setuptools import setup
from synker import get_version


with open('README.md', 'r') as f:
    readme = f.read()

with open('LICENSE', 'r') as f:
    license = f.read()


setup(
    name='synker',
    version=get_version(),
    description='A lightweight automated backup utility written in Python.',
    long_description=readme,
    author='Robert Taylor',
    author_email='rtaylor@pyrunner.com',
    url='https://bitbucket.org/rtaylor/synker',
    license=license,
    install_requires=['docopt'],
    py_modules=['synker'],
    entry_points={
        'console_scripts': ['synker = synker:main'],
        },
    )

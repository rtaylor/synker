synker
======

A lightweight automated backup utility written in Python (3.6) for Ubuntu (16.04). The utility creates archives for MySQL databases and files from local or remote machines. As time passes, older archives are discarded according to a user-defined schedule.


Installation
------------

To install the most recent version:

	sudo pip3 install docopt
	sudo pip3 install git+https://rtaylor@bitbucket.org/rtaylor/synker.git

To install the development branch, append `@develop` to the command above.


Development Setup
-----------------

1. Download or clone the project:

        git clone https://rtaylor@bitbucket.org/rtaylor/synker.git
        cd synker

2. Setup and activate a Python 3.6 virtual environment:

        make install-venv

3. Run the tests and try building the project in-place:

		make test
		make test-build


MySQL User
----------

It is recommended to create a MySQL backup user instead of using the root user to automate MySQL dumps.

	mysql -u root -p -e "CREATE USER 'backup'@'localhost' IDENTIFIED BY '***';\
	    GRANT SELECT, RELOAD, FILE, SUPER, LOCK TABLES, SHOW VIEW ON *.* TO 'backup'@'localhost';"


Basic Usage
-----------

*Synker* is controlled by a configuration file found in the home directory. The first time you run *synker* at the command-line, it will create a boilerplate configuration file at `~/.synker/conf.py`. It looks something like this:

```python
{
    "project": {

        ## List of target directories to save the new backup files.
        "archives": [
            ## "/path/to/archive/dir/",
        ],

        ## Configure the backup schedule or use a preset:
        ## "smart", "daily", "weekly".
        "schedule": "smart",

        ## Dict of local system paths to include in the backup.
        "local": {
            ## "name": "/path/to/source",
        },

        ## Dict of MySQL dumps to include in the backup.
        "mysql": {
            ## "name": ("database", "username", "password", "host"),
        },

        ## Dict of remote paths (via SSH) to include in the backup.
        "ssh": {
            ## "name": ("user", "host", "/path/to/source", "port"),
        },
    },
}
```

Simply define one or more *backup projects* in the configuration file with the desired output directory (`"archives"`), schedule (`"schedule"`), MySQL dumps (`"mysql"`), local paths (`"local"`), and/ or remote paths (`"ssh"`).

To run *synker* periodically you must create a cronjob for it. First, determine where the *synker* executable was installed:

	whereis synker  # synker: /usr/local/bin/synker

Then, to create a user-level cronjob, execute `crontab -e` and enter a line like:

	*/30 * * * * /usr/local/bin/synker

The above job should execute the *synker* executable every 30 minutes. Each time it executes, *synker* checks its configured schedule and performs backups as needed.


Schedules
---------

A schedule is a list of positive integer pairs: (iterations, minutes). Here's what the `"smart"` schedule looks like:

	[(5, 1440), (5, 10080), (10, 43200), (50, 144000)]

Each pair declares a number of backups (iterations) and the approximate amount of time that should separate those backups (in minutes). So, the pair `(5, 1440)` is equivalent to *five backups, each separated by 1440 minutes*. The entire `"smart"` schedule, stated in English, goes something like this:

	Keep at most 5 daily backups; followed by at most 5 weekly backups; followed by at most 10 30-day backups; followed by at most 50 100-day backups.

To figure out which backups to keep and which ones to discard, a very simple binning algorithm is used. Examine the code to understand exactly how it works.
